# Full-Stack JavaScript Architecture
Opinionated project architecture for Full-Stack JavaScript Applications.

## About
Using JavaScript for full-stack has always been a challenge especially with architecting various pieces of the application, choosing technologies and managing devOps. This project provides a base for typical project consisting a Landing Website, Web and Mobile Applications, API service and easy deployment of these services. This project uses a microservice architecture where all individual project runs as a service (container).

A typical product (SaaS, etc.) usually consists of following services:
- Web Application
    - Your actual application for your customers to use
    - Desktop browser
    - Tablet and mobile browser via responsive design

## Core Structure
    fsja
      ├── backend
      │   ├── api
      │   │   > NodeJS
      │   │   > PORT 8000
      │   │   > api.example.com
      │   │
      │   ├── database
      │   │   > MongoDB
      │   │   > PORT 27017
      │   │
      │   └── proxy
      │       > NGINX
      │       > PORT 80 / 443
      │
      ├── frontend
      │   ├── app
      │       └── web
      │           > React
      │           > Single page application
      │           > PORT 5000
      │           > app.example.com
      │
      └── README.md (you are here)

## Stack

### Backend
- API
    - NodeJS
    - Express
- Database
    - MongoDB
- Proxy
    - NGINX

### Frontend
- Web
    - React
    - Redux
    - React Router
    - Material UI
